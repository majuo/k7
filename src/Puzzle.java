import java.util.*;

public class Puzzle {

   private String addend1;
   private String addend2;
   private String sum;

   public Puzzle(String add1, String add2, String sum) {
      addend1 = add1;
      addend2 = add2;
      this.sum = sum;
   }

   public ArrayList<HashMap<String, Integer>> solutions = new ArrayList<>();

   /**
    * Convert word to a number using given keys and values.
    */
   public static Long strToInt(String word, HashMap<String, Integer> keys){
      long result = 0L;
      for (int i = 0; i < word.length(); i++) {
         result += (long)(keys.get(String.valueOf(word.charAt(i))) * Math.pow(10, word.length() - i - 1));
      }
      return result;
   }

   public void getSolutions(){
       System.out.println("Solutions for puzzle '" + addend1 + " + " + addend2 + " = " + sum + "' :");
       for (HashMap<String, Integer> solution : solutions) {
           System.out.println("\n" + solution);
       }
   }


   /**
    * Try all possible combinations.
    * Method recursively tries to set up all digits with all keys.
    */
   public void backtrack(int index, ArrayList<String> keySet, HashMap<String, Integer> keys) {
      for (int i = 0; i < 10; i++) {
         //Check if values already contain the number - all letters should have different numbers
         if (keys.values().contains(i)) {
            for (int j = index; j < keySet.size(); j++) {
               keys.put(keySet.get(j), null);
            }
            continue;
         }
         keys.put(keySet.get(index), i);
         //If it is the last key in keySet, then we have full new combination. Check the condition and then (maybe) add to solutions.
         if (index == keySet.size() - 1) {
            if (strToInt(addend1, keys) + strToInt(addend2, keys) == strToInt(sum, keys)) {
               solutions.add(new HashMap<>(keys));
            }
         } else {
            backtrack(index + 1, keySet, keys);
         }
         /*
         * After trying all digits for a key, key's value(and values of all the keys that follow current one) are turned into
         * null so that method could correctly check combinations for having same digits for different keys.
         */
         if (i == 9) {
            for (int j = index; j < keySet.size(); j++) {
               keys.put(keySet.get(j), null);
            }
         }
      }
   }


   private ArrayList<HashMap<String, Integer>> checkForBeginningZeroes() {
      ArrayList<HashMap<String, Integer>> res = new ArrayList<>();
      for (HashMap<String, Integer> solution : solutions) {
         if (solution.get(Character.toString(addend1.charAt(0))) != 0 &&
                 solution.get(Character.toString(addend2.charAt(0))) != 0 &&
                 solution.get(Character.toString(sum.charAt(0))) != 0) {
            res.add(solution);
         }
      }
      return res;
   }

   public void getKeySet(HashSet<String> keySet) {
       for (int i = 0; i < addend1.length(); i++) {
           keySet.add(Character.toString(addend1.charAt(i)));
       }
       for (int i = 0; i < addend2.length(); i++) {
           keySet.add(Character.toString(addend2.charAt(i)));
       }
       for (int i = 0; i < sum.length(); i++) {
           keySet.add(Character.toString(sum.charAt(i)));
       }
   }


   public ArrayList<HashMap<String, Integer>> solve() {
       //Constructing set with all the keys.
      HashSet<String> keySet = new HashSet<>();
      getKeySet(keySet);

      //Constructing HashMap with all the keys. Default values - null.
      HashMap<String, Integer> keys = new HashMap<>();
      for (String key:keySet) {
         keys.put(key, null);
      }

      //Solving the puzzle
      backtrack(0, new ArrayList<>(keySet), keys);

      //Remove all solutions where value for the first letter in a word is 0. Numbers cant start with 0.
      solutions = checkForBeginningZeroes();

      //Printing results
      if (solutions.size() == 0) System.out.println("There are no solutions for puzzle '" + addend1 + " + " + addend2 + " = " + sum + "'.");
      else System.out.println("Number of solutions: " + solutions.size() + "\nHere is the first solution: " + solutions.get(0));

      return solutions;
   }

   /** Solve the word puzzle.
    * @param args three words (addend1 addend2 sum)
    */
   public static void main (String[] args) {
      Puzzle one = new Puzzle(args[0], args[1], args[2]);
      one.solve();
      //To get all solutions, call Puzzle object's getSolutions method.
   }
}
